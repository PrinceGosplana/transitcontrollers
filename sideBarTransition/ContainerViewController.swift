//
//  ContainerViewController.swift
//  sideBarTransition
//
//  Created by Александр Исаев on 29.04.15.
//  Copyright (c) 2015 Private. All rights reserved.
//

import UIKit

let TOP_VIEW_HEIGHT = CGFloat(64.0)

class ContainerViewController: UIViewController {
  
  let topViewController: UIViewController!
  let mainViewController: UIViewController!
  
  init(topView: UIViewController, main: UIViewController) {
    topViewController = topView
    mainViewController = main
    super.init(nibName: nil, bundle: nil)
  }
  
  required init(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    addChildViewController(mainViewController)
    view.addSubview(mainViewController.view)
    mainViewController.didMoveToParentViewController(self)
    
    addChildViewController(topViewController)
    view.addSubview(topViewController.view)
    topViewController.didMoveToParentViewController(self)
    
    topViewController.view.frame = CGRect(
      x: 0.0,
      y: 0.0,
      width: UIScreen.mainScreen().bounds.size.width,
      height: TOP_VIEW_HEIGHT)
    
    mainViewController.view.frame = CGRect(
      x: 0.0,
      y: TOP_VIEW_HEIGHT,
      width: topViewController.view.frame.width,
      height: UIScreen.mainScreen().bounds.size.height - TOP_VIEW_HEIGHT)
  }
}
