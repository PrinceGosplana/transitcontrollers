//
//  AppDelegate.swift
//  sideBarTransition
//
//  Created by Александр Исаев on 29.04.15.
//  Copyright (c) 2015 Private. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  var topView: UIView!

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    
    application.statusBarStyle = UIStatusBarStyle.LightContent
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    
    let viewController = storyboard.instantiateViewControllerWithIdentifier("firstVC") as! ViewController
    let topVC = storyboard.instantiateViewControllerWithIdentifier("topView") as! UIViewController

    
    let containerVC = ContainerViewController(topView: topVC, main: viewController)
    
    self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
    self.window!.rootViewController = containerVC
    self.window!.backgroundColor = UIColor.whiteColor()
    self.window!.makeKeyAndVisible()

    return true
  }
}

